#ifndef HASHBLOCK_H
#define HASHBLOCK_H

#include "uint256.h"
#include "argon2d.h"
#include <string>

using namespace std;


template <typename T>
inline uint256 Hash2(const T pbegin, const T pend) {

    size_t outlen = 64;
    unsigned char* out = new unsigned char[outlen]; // output length is 256bit data

    static unsigned char pblank[1];
    const void* in = (pbegin == pend ? pblank : static_cast<const void*>(&pbegin[0]));

    size_t inlen = (pend - pbegin) * sizeof(pbegin[0]);

    unsigned char one_array[256];
    memset(one_array, 1, 256);

    size_t t_cost = 3; /* t_cost is number of iterations, and can be between 0 and 2^32 -1 */
    size_t m_cost = 1 << 18; /* m_cost is memory size representing number of KiB */

    /* t_const and m_const values are same as in Argon2d-github exapmle */

    PHS((void*)out, outlen, (const void*)in, inlen, (const void*)one_array, 16, t_cost, m_cost);


    std::vector<unsigned char> tmp(out, out + 32/sizeof(*out));

    delete [] out;

    uint256 return_hash(tmp);

    return return_hash;

}





#endif // HASHBLOCK_H
